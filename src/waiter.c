/*******************************************************************************
 * PiBox application manager
 *
 * waiter.c:  Wait on child processes and restart launcher, if available.
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define WAITER_C

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <ifaddrs.h>
#include <string.h>
#include <errno.h>
#include <uuid/uuid.h>

#include "appmgr.h"

static int waiterIsRunning = 0;
static pthread_mutex_t waiterMutex = PTHREAD_MUTEX_INITIALIZER;
static pthread_cond_t condWaiter = PTHREAD_COND_INITIALIZER;
static pthread_t waiterThread;
static pid_t waiterPID = -1;

/*========================================================================
 * Name:   setPID
 * Prototype:  int setPID( pid_t )
 *
 * Description:
 * Thread-safe setting of PID to wait on.
 *========================================================================*/
void
setPID( pid_t pid )
{
    pthread_mutex_lock( &waiterMutex );
    waiterPID = pid;
    pthread_cond_signal( &condWaiter);
    pthread_mutex_unlock( &waiterMutex );
}

/*========================================================================
 * Name:   isWaiterRunning
 * Prototype:  int isWaiterRunning( void )
 *
 * Description:
 * Thread-safe read of waiterIsRunning variable.
 *========================================================================*/
static int
isWaiterRunning( void )
{
    int status;
    pthread_mutex_lock( &waiterMutex );
    status = waiterIsRunning;
    pthread_mutex_unlock( &waiterMutex );
    return status;
}

/*========================================================================
 * Name:   setWaiterRunning
 * Prototype:  int setWaiterRunning( void )
 *
 * Description:
 * Thread-safe set of waiterIsRunning variable.
 *========================================================================*/
static void
setWaiterRunning( int val )
{
    pthread_mutex_lock( &waiterMutex );
    waiterIsRunning = val;
    pthread_mutex_unlock( &waiterMutex );
}

/*========================================================================
 * Name:   startLauncher
 * Prototype:  void startLauncher()
 *
 * Description:
 * Start the launcher application.
 *========================================================================*/
void
startLauncher( )
{
    struct stat stat_buf;
    char *cmdLine, *dup;
    char *ptr;

    waiterPID = -1;
    if ( cliOptions.launcher != NULL )
    {
        dup = g_strdup(cliOptions.launcher);
        cmdLine = g_strdup(cliOptions.launcher);
        ptr = strtok(cmdLine, " ");

        // Start launcher, if it's available
        if ( stat(ptr, &stat_buf) == 0 )
        {
            piboxLogger(LOG_INFO, "Launcher found: %s\n", ptr);
            queueMsg( MT_START, strlen(dup), NULL, dup);
        }
        else
            piboxLogger(LOG_INFO, 
                "Launcher not found: %s. No initial application will be started.\n", ptr);
        g_free(cmdLine);
    }
    else
    {
        // Start launcher, if it's available
        dup = g_strdup(LAUNCHER);
        if ( stat(dup, &stat_buf) == 0 )
        {
            piboxLogger(LOG_INFO, "Launcher found: %s\n", dup);
            queueMsg( MT_START, strlen(dup), NULL, dup );
        }
        else
            piboxLogger(LOG_INFO, 
                "Launcher not found: %s. No initial application will be started.\n", dup);
    }
}

/*========================================================================
 * Name:   waiter
 * Prototype:  void waiter( CLI_T * )
 *
 * Description:
 * Thread that waits on running child.  This is done to avoid zombies.
 *
 * Input Arguments:
 * void *arg    Unused
 *========================================================================*/
static void *
waiter( void *arg )
{
    pid_t localPID;
    pid_t returnPID;
    int status;

    waiterIsRunning = 1;
    while( isCLIFlagSet(CLI_SERVER) ) 
    {
        // if there's a child pid, wait on it.
        pthread_mutex_lock( &waiterMutex );
        while (waiterPID == -1)
        {
            pthread_cond_wait( &condWaiter, &waiterMutex );
        }
        /* Shutdown will have set this already. */
        localPID = waiterPID;
        pthread_mutex_unlock( &waiterMutex );
        if( !isCLIFlagSet(CLI_SERVER) )
            break;

        if (localPID != -1)
        {
            piboxLogger(LOG_INFO, "Waiting on pid: %d\n", localPID);
            returnPID = waitpid(-1, &status, 0);
            if ( returnPID == localPID )
            {
                piboxLogger(LOG_INFO, "%d exited.\n", localPID);
                clearCurrent();
                waiterPID = -1;
            }
            else if ( returnPID == -1 )
                piboxLogger(LOG_ERROR, "waitpid returned error: %s\n", strerror(errno));
        }
    }

    piboxLogger(LOG_INFO, "%s: Waiter thread is exiting.\n", PROG);
    waiterIsRunning = 0;
    return(0);
}

/*========================================================================
 * Name:   startWaiter
 * Prototype:  void startWaiter( void )
 *
 * Description:
 * Setup thread to handle inbound messages.
 *
 * Notes:
 * Threads run until configs->serverEnabled = 0.  See shutdownWaiter().
 *========================================================================*/
void
startWaiter( void )
{
    int rc;

    /* Create a thread to handle inbound messages. */
    rc = pthread_create(&waiterThread, NULL, &waiter, (void *)NULL);
    if (rc)
    {
        piboxLogger(LOG_ERROR, "%s: Failed to create Waiter thread: %s\n", PROG, strerror(rc));
        exit(-1);
    }
    piboxLogger(LOG_INFO, "%s: Started Waiter thread.\n", PROG);
    return;
}

/*========================================================================
 * Name:   shutdownWaiter
 * Prototype:  void shutdownWaiter( void )
 *
 * Description:
 * Shut down message processing thread.
 *========================================================================*/
void
shutdownWaiter( void )
{
    int timeOut = 1;

    setPID(-2);
    while ( isWaiterRunning() )
    {
        sleep(1);
        timeOut++;
        if (timeOut == 60)
        {
            piboxLogger(LOG_ERROR, "%s: Timed out waiting on Waiter thread to shut down.\n", PROG);
            return;
        }
    }
    pthread_detach(waiterThread);
    piboxLogger(LOG_INFO, "%s: Waiter has shut down.\n", PROG);
}

