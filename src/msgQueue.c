/*******************************************************************************
 * PiBox application manager
 *
 * msgQueue.c:  Message Queue Management
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define MSGQUEUE_C

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <string.h>
#include <errno.h>
#include <uuid/uuid.h>

#include "appmgr.h"

// The head of the message queues.
PIBOX_MSG_T *inQueue = NULL;    // Inbound messages, yet to be processed.
PIBOX_MSG_T *procQueue = NULL;  // Processed messages, but not yet expired.

/*
 *========================================================================
 *========================================================================
 *
 * Queue Management
 *
 *========================================================================
 *========================================================================
 */

/*========================================================================
 * Name:   queueSize
 * Prototype:  int queueSize( int )
 *
 * Description:
 * Retrieve the current size of the queue.
 *
 * Arguments:
 * int queueID      Either Q_IN or Q_PROC
 *
 * Returns:
 * The size of the queue.
 *========================================================================*/
int
queueSize( int queueID )
{
    int             count = 0;
    PIBOX_MSG_T     *ptr  = NULL;

    pthread_mutex_lock( &queueMutex );
    if ( queueID == Q_IN )
        ptr  = inQueue;
    else
        ptr  = procQueue;
    while ( ptr!=NULL )
    {
        count++;
        ptr = ptr->next;
    }
    pthread_mutex_unlock( &queueMutex );
    return count;
}

/*========================================================================
 * Name:   freeMsgNode
 * Prototype:  void freeMsgNode( PIBOX_MSG_T * )
 *
 * Description:
 * Free a message queue node.  It should already have been popped from its queue.
 *
 * Input Arguments:
 * PIBOX_MSG_T *node     Node to free.
 *========================================================================*/
void
freeMsgNode( PIBOX_MSG_T *node )
{
    if ( node == NULL )
        return;
    if ( node->payload != NULL )
        free(node->payload);
    free(node);
}

/*========================================================================
 * Name:   popMsgQueue
 * Prototype:  PIBOX_MSG_T *popMsgQueue( void )
 *
 * Description:
 * Pull the next message from the inbound queue, if possible.
 *
 * Returns
 * Pointer to a node if found, NULL otherwise.  If found, caller is responsible
 * for freeing the node using freeMsgNode().
 *========================================================================*/
PIBOX_MSG_T *
popMsgQueue( void )
{
    PIBOX_MSG_T    *ptr  = NULL;
    PIBOX_MSG_T    *next = NULL;

    pthread_mutex_lock( &queueMutex );
    ptr = inQueue;
    if ( ptr != NULL )
    {
        piboxLogger(LOG_INFO, "Popped a message.\n");
        next = ptr->next;
    }
    inQueue = next;
    pthread_mutex_unlock( &queueMutex );
    return ( ptr );
}

/*========================================================================
 * Name:   queueMsg
 * Prototype:  void queueMsg( int, int, char *, char *, int)
 *
 * Description:
 * Queue a message for processing.
 * The new message goes on the end of the list.
 *
 * Input Arguments:
 * int header            Header for the message, which defines action, type, etc.
 * int size              Size of the payload.
 * char *tag             The unique tag for this message.
 * char *payload         Payload pulled from message.
 *========================================================================*/
void 
queueMsg( int header, int size, char *tag, char *payload)
{
    PIBOX_MSG_T    *ptr  = NULL;
    PIBOX_MSG_T    *last = NULL;

    pthread_mutex_lock( &queueMutex );
    ptr = inQueue;
    while (ptr != NULL)
    {
        last = ptr;
        ptr = ptr->next;
    }
    ptr = (PIBOX_MSG_T *)malloc(sizeof(PIBOX_MSG_T));
    ptr->next = NULL;
    ptr->header = header;
    ptr->size = size;
    ptr->payload = payload;

    if ( last == NULL )
        inQueue = ptr;
    else
        last->next = ptr;

    pthread_mutex_unlock( &queueMutex );
    piboxLogger(LOG_INFO, "Added to inbound queue, new size: %d\n", queueSize(Q_IN));

    /* Wake the processor */
    if ( sem_post(&queueSem) != 0 )
        piboxLogger(LOG_ERROR, "Failed to post queueSem semaphore.\n");
}

/*========================================================================
 * Name:   clearQueue
 * Prototype:  clearQueue( void )
 *
 * Description:
 * Clear the inbound message queue.
 *========================================================================*/
void
clearMsgQueue( void )
{
    PIBOX_MSG_T *msg = NULL;
    while ( (msg=popMsgQueue()) != NULL )
        freeMsgNode(msg);
}

