/*******************************************************************************
 * PiBox application manager
 *
 * waiter.h:  Handle log messages
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#ifndef WAITER_H
#define WAITER_H

/*========================================================================
 * Defined values
 *=======================================================================*/

/*========================================================================
 * TYPEDEFS
 *=======================================================================*/


/*========================================================================
 * Prototypes
 *=======================================================================*/
#ifndef WAITER_C
extern void setPID( pid_t );
extern void startLauncher();
extern void startWaiter();
extern void shutdownWaiter();
#endif /* !WAITER_C */
#endif /* !WAITER_H */
