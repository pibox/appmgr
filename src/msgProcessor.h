/*******************************************************************************
 * PiBox application manager
 *
 * msgProcessor.c:  Process inbound messages.
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#ifndef MSGPROCESSOR_H
#define MSGPROCESSOR_H

#include <pthread.h>

#define INPORT                  13912

/*========================================================================
 * TYPEDEFS
 *=======================================================================*/


/*========================================================================
 * Variables
 *=======================================================================*/

/*========================================================================
 * Prototypes
 *=======================================================================*/
#ifndef MSGPROCESSOR_C

extern void startMsgProcessor( void );
extern void shutdownMsgProcessor( void );

#endif /* !MSGPROCESSOR_C */

/*========================================================================
 * Variable definitions
 *=======================================================================*/

#endif /* !MSGPROCESSOR_H */
