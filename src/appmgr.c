/*******************************************************************************
 * PiBox application manager
 *
 * main.c:  Setup and initialization
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define MAIN_C

// System headers
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <signal.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <semaphore.h>
#include <pibox/utils.h>

// Program headers
#include "appmgr.h"

static sem_t mainSem;

/*
 * ========================================================================
 * Name:   sigHandler
 * Prototype:  static void sigHandler( int sig, siginfo_t *si, void *uc )
 *
 * Description:
 * Handles program signals.
 *
 * Input Arguments:
 * int       sig    Signal that cause callback to be called
 * siginfo_t *si    Structure that includes timer over run information and timer ID
 * void      *uc    Unused but required with sigaction handlers.
 * ========================================================================
 */
static void
sigHandler( int sig, siginfo_t *si, void *uc )
{
    switch (sig)
    {
        case SIGINT:
        case SIGQUIT:
        case SIGSTOP:
        case SIGHUP:
        case SIGTERM:
            /* Bring it down gracefully. */
            if ( sem_post(&mainSem) != 0 )
                piboxLogger(LOG_ERROR, "Failed to post app semaphore.\n");
            break;
    }
}

/*
 * ========================================================================
 * Handle shutdown processing outside of daemon
 * ========================================================================
 */
static void
shutdown(int signo)
{
    switch (signo)
    {
        case SIGINT:
        case SIGQUIT:
        case SIGSTOP:
        case SIGHUP:
        case SIGTERM:
            /* Bring it down gracefully. */
            if ( sem_post(&mainSem) != 0 )
                piboxLogger(LOG_ERROR, "Failed to post app semaphore.\n");
            break;
    }
}

/*
 * ========================================================================
 * Set up a lock file so only one instance can run.
 * ========================================================================
 */
static void
lockit()
{
    char pidStr[7];
    char *lockFile;

    if ( isCLIFlagSet( CLI_TEST) )
        lockFile = LOCK_FILE_T;
    else
        lockFile = LOCK_FILE;

    cliOptions.lockfd = open(lockFile,O_RDWR|O_CREAT,0640);
    if ( cliOptions.lockfd<0 )
    {
        fprintf(stderr, "%s: Failed to get lock file.  Exiting.\n", PROG);
        exit(1);
    }

    if (lockf(cliOptions.lockfd,F_TLOCK,0)<0)
    {
        /* only first instance continues */
        fprintf(stderr, "%s: is already running.\n", PROG);
        exit(0);
    }

    /* Save child pid to lockfile */
    sprintf(pidStr,"%d\n",getpid());
    if ( write(cliOptions.lockfd, pidStr, strlen(pidStr)) == -1 )
        fprintf(stderr, "Failed to save child pid to lockfile.\n");
}

/*
 * ========================================================================
 * Handle daemonizing of the application.
 * See http://www.enderunix.org/docs/eng/daemon.php
 * ========================================================================
 */
static void
daemonize( void )
{
    int                 pid, fd;
    struct sigaction    sa;
    int                 s = 0;
    sigset_t            set;

    pid = fork();

    /* On error, print a message and exit */
    if ( pid<0 )
    {
        fprintf(stderr, "%s: Failed to daemonize: %s\n", PROG, strerror(errno));
        exit(1);
    }

    /* If this is the parent, we exit immediately. */
    if ( pid>0 )
    {
        fprintf(stderr, "%s: parent process is exiting.\n", PROG);
        exit(1);
    }

    /* The child needs some work to make it a daemon. */

    /* Get our own process group and detach the controlling terminal. */
    setsid();

    /* File creation mask - for safety sake */
    umask(027);

    /* Change to a meaningful working directory. */
    mkdir(RUN_DIR, 0700);
    if ( chdir(RUN_DIR) == -1 )
        fprintf(stderr, "Failed to change to %s\n", RUN_DIR);

    /* Set up a lock file so only one instance can run. */
    lockit();

    /* Setup for basic signal handling */
    sigemptyset(&set);
    sigaddset(&set, SIGUSR1);
    s = pthread_sigmask(SIG_BLOCK, &set, NULL);
    if (s != 0)
    {
        fprintf(stderr, "%s: Failed to block SIGUSR1 in main thread.\n", PROG);
        exit(1);
    }
    sa.sa_flags = SA_SIGINFO;
    sa.sa_sigaction = sigHandler;
    sigemptyset(&sa.sa_mask);
    if (sigaction(SIGHUP, &sa, NULL) == -1)
    {
        fprintf(stderr, "%s: Failed to setup signal handling for SIGHUP.\n", PROG);
        exit(1);
    }
    sa.sa_flags = SA_SIGINFO;
    sa.sa_sigaction = sigHandler;
    sigemptyset(&sa.sa_mask);
    if (sigaction(SIGTERM, &sa, NULL) == -1)
    {
        fprintf(stderr, "%s: Failed to setup signal handling for SIGTERM.\n", PROG);
        exit(1);
    }

    /* Close inherited descriptors, then get our own. */
    for (fd=getdtablesize(); fd>=0; --fd)
        close(fd);
    fd = open("/dev/null", O_RDWR);    stdin  = fdopen(fd, "r"); /* stdin  */
    fd = open("/dev/console", O_RDWR); stderr = fdopen(fd, "w"); /* stdout */
    fd = dup(fd);                      stderr = fdopen(fd, "w"); /* stderr */
}


/*
 * ========================================================================
 * Name:   main
 * Prototype:  static void main( int, char ** )
 *
 * Description:
 * Program initialization
 *
 * Input Arguments:
 * int       argc   Number of command line arguments
 * char      **argv Command line arguments
 * ========================================================================
 */
int
main( int argc, char **argv)
{
    struct stat stat_buf;

    // Load saved configuration and parse command line
    initConfig();
    parseArgs(argc, argv);

    // Setup logging
    piboxLoggerInit(cliOptions.logFile);
    piboxLoggerVerbosity(cliOptions.verbose);

    /* Load the environment variables file, if any. */
    piboxLoadEnv(NULL);

    // Daemonize and setup signal handling
    daemonEnabled = 1;
    if ( cliOptions.flags & CLI_DAEMON )
    {
        piboxLogger(LOG_INFO,"Daemonizing...\n");
        daemonize();
    }
    else
    {
        piboxLogger(LOG_INFO,"Not a daemon.\n");
        lockit();

        /* Handle signals */
        signal(SIGINT, shutdown);
        signal(SIGQUIT, shutdown);
        signal(SIGSTOP, shutdown);
        signal(SIGHUP, shutdown);
        signal(SIGTERM, shutdown);
    }
    piboxLogger(LOG_INFO, "PID: %d\n", getpid());

    // Start threads
    startMsgProcessor();
    startQueueProcessor();   // Process recently received requests
    startWaiter();           // Deal with stopped apps

    // Start launcher, if it's available
    startLauncher();

    /* Set up the semaphore used to process inbound messages. */
    if (sem_init(&mainSem, 0, 0) == -1)
    {
        piboxLogger(LOG_ERROR, "Failed to get semaphore: %s\n", strerror(errno));
        goto _main_exit;
    }

    // Spin
    piboxLogger(LOG_INFO, "Main thread...\n");

    /* Block waiting for at least one message to be added to the queue. */
    sem_wait(&mainSem);

    /* Clean up the semaphore used for this thread. */
    sem_destroy(&mainSem);

_main_exit:
    piboxLogger(LOG_INFO, "Exiting...\n");

    // Gracefully cleanup
    unsetCLIFlag( CLI_SERVER );
    shutdownWaiter();
    shutdownMsgProcessor();
    shutdownQueueProcessor();
    clearMsgQueue();
    if ( !isCLIFlagSet( CLI_TEST) )
        stopLauncher();
    piboxLogger(LOG_INFO, "b'bye!\n");
    piboxLoggerShutdown();

    // Get rid of lock file
    close(cliOptions.lockfd);
    if ( isCLIFlagSet( CLI_TEST) )
        unlink(LOCK_FILE_T);
    else
        unlink(LOCK_FILE);

    // b'bye!
    return(0);
}
