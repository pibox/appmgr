/*******************************************************************************
 * PiBox application manager
 *
 * main.h:  Setup and initialization controller
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#ifndef MAIN_H
#define MAIN_H

#include <unistd.h>
#include <time.h>
#include <glib.h>
#include <pibox/log.h>

/*========================================================================
 * Type definitions
 *=======================================================================*/
typedef struct _pibox_msg {
    struct _pibox_msg   *next;
    int                 header;         // 4 byte header: start or stop bits
    int                 size;           // size of payload
    char                *payload;       // payload (path and filename)
} PIBOX_MSG_T;

/*========================================================================
 * Defined values
 *=======================================================================*/
#define PROG        "appmgr"
#define MAXBUF      4096

// Generic return codes
#define N_OK        1
#define N_PASS      2
#define N_FAIL      3

// Message types (byte 1 of PIBOX_MSG_T header)
#define MT_START    1
#define MT_STOP     2
// #define MT_KEY      3

#define Q_IN        1       // The inbound message queue
#define Q_PROC      2       // The processed message queue

// Default launcher location
#define LAUNCHER    "/usr/bin/launcher"
#define LAUNCHER_L  "/usr/bin/launcher -v3 -l /var/log/launcher.log"

/*========================================================================
 * Globals
 *=======================================================================*/
#ifdef MAIN_C
int     daemonEnabled = 0;
char    errBuf[256];
#else
extern int      daemonEnabled;
extern char     errBuf[];
#endif /* MAIN_C */

/*========================================================================
 * Include other headers
 *=======================================================================*/
#include <pibox/log.h>
#include <pibox/utils.h>
#include "cli.h"
#include "msgProcessor.h"
#include "queueProcessor.h"
#include "msgQueue.h"
#include "waiter.h"

#endif /* !MAIN_H */

