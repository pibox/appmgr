/*******************************************************************************
 * PiBox application manager
 *
 * queueProcessor.c:  Process queued messages (re: start/stop processes).
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#ifndef QUEUEPROCESSOR_H
#define QUEUEPROCESSOR_H

#include <pthread.h>
#include <semaphore.h>

/*========================================================================
 * TYPEDEFS
 *=======================================================================*/


/*========================================================================
 * Variables
 *=======================================================================*/
#ifdef QUEUEPROCESSOR_C
sem_t           queueSem;
#else
extern sem_t           queueSem;
#endif /* QUEUEPROCESSOR_C */

/*========================================================================
 * Prototypes
 *=======================================================================*/
#ifndef QUEUEPROCESSOR_C

extern void clearCurrent( void );
extern void stopCurrent( void );
extern void startQueueProcessor( void );
extern void wakeQueueProcessor( void );
extern void shutdownQueueProcessor( void );
extern void stopLauncher( void );

#endif /* !QUEUEPROCESSOR_C */

/*========================================================================
 * Variable definitions
 *=======================================================================*/

#endif /* !QUEUEPROCESSOR_H */
