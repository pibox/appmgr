/*******************************************************************************
 * PiBox application manager
 *
 * cli.c:  Command line parsing
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define CLI_C

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <sys/stat.h>
#include <sched.h>
#include <sys/mman.h>
#include <errno.h>
#include <signal.h>
#include <pthread.h>
#include <pibox/utils.h>

#include "appmgr.h"

PRIV_T *privHead = NULL;

static pthread_mutex_t cliOptionsMutex = PTHREAD_MUTEX_INITIALIZER;

/*========================================================================
 * Name:   loadConfig
 * Prototype:  void loadConfig( void )
 *
 * Description:
 * Read the configuration file and set options accordingly.
 *========================================================================*/
void
loadConfig( void )
{
    char            *tsave     = NULL;
    char            *tok       = NULL;
    struct stat     stat_buf;
    char            buf[MAXBUF];
    char            *filename;
    FILE            *fd;
    PRIV_T          *priv;
    PRIV_T          *last;

    if ( isCLIFlagSet( CLI_TEST) )
        cliOptions.filename = F_CFG_T;

    // Check that the configuration file exists.
    if ( stat(cliOptions.filename, &stat_buf) == 0 )
    {
        // Open the config file.
        fd = fopen(cliOptions.filename, "r");
        if ( fd == NULL )
        {
            fprintf(stderr, "Failed to open %s: %s\n", cliOptions.filename, strerror(errno));
            return;
        }

        // Parse file.
        // Config file uses name:value pairs
        while ( fgets(buf, MAXBUF-1, fd) != NULL )
        {   
            piboxStripNewline(buf);
            // fprintf(stderr, "Config file line: %s\n", buf);
            tok = strtok_r(buf, ":", &tsave);

            // Debug level
            if ( strcasecmp(tok, "verbose") == 0 )
            {   
                tok = strtok_r(NULL, ":", &tsave);
                cliOptions.verbose = atoi(tok);
                fprintf(stderr, "Set verbose level to: %d\n", cliOptions.verbose);
            }

            // Debug file
            else if ( strcasecmp(tok, "logfile") == 0 )
            {   
                tok = strtok_r(NULL, ":", &tsave);
                cliOptions.logFile = g_strdup(tok);
                cliOptions.flags |= CLI_LOGTOFILE;
                fprintf(stderr, "Log file: %s\n", cliOptions.logFile);
            }

            // Command for the launcher
            else if ( strcasecmp(tok, "launcher") == 0 )
            {   
                tok = strtok_r(NULL, ":", &tsave);
                cliOptions.launcher = g_strdup(tok);
                fprintf(stderr, "Launcher command: %s\n", cliOptions.launcher);
            }
        }

        fclose(fd);
    }
    else
        fprintf(stderr, "No such config: %s\n", cliOptions.filename);

    // Read the privileged app file.
    if ( isCLIFlagSet( CLI_TEST) )
        filename = PRIV_FD;
    else
        filename = PRIV_F;

    // Check that the configuration file exists.
    if ( stat(filename, &stat_buf) == 0 )
    {
        // Open the config file.
        fd = fopen(filename, "r");
        if ( fd == NULL )
        {
            fprintf(stderr, "Failed to open %s: %s\n", filename, strerror(errno));
            return;
        }

        while ( fgets(buf, MAXBUF-1, fd) != NULL )
        {   
            piboxStripNewline(buf);
            fprintf(stderr, "Privileged app: %s\n", buf);
            priv = (PRIV_T *) malloc(sizeof(PRIV_T));
            memset(priv, 0, sizeof(PRIV_T));
            priv->appname = strdup(buf);

            if ( privHead == NULL )
                privHead = priv;
            else
                last->next = priv;
            last = priv;
        }
    }
}

/*========================================================================
 * Name:   parseArgs
 * Prototype:  void parseArgs( int, char ** )
 *
 * Description:
 * Parse the command line
 *
 * Input Arguments:
 * int argc         Number of command line arguments
 * char **argv      Command line arguments to parse
 *========================================================================*/
void
parseArgs(int argc, char **argv)
{
    int opt;

    /* Suppress error messages from getopt_long() for unrecognized options. */
    opterr = 0;

    /* Parse the command line. */
    while ( (opt = getopt(argc, argv, CLIARGS)) != -1 )
    {
        switch (opt)
        {
            /* Daemon mode. */
            case 'd':
                cliOptions.flags |= CLI_DAEMON;
                break;

            /* Enable configuration file. */
            case 'f':
                cliOptions.flags |= CLI_FILE;
                cliOptions.filename = optarg;
                break;

            /* Enable logging to local file. */
            case 'l':
                cliOptions.flags |= CLI_LOGTOFILE;
                cliOptions.logFile = optarg;
                break;

            /* -T: Use test files. */
            case 'T':
                cliOptions.flags |= CLI_TEST;
                break;

            /* -v: Verbose output (verbose is a library variable). */
            case 'v':
                cliOptions.verbose = atoi(optarg);
                break;

            default:
                printf("%s\nVersion: %s - %s\n", PROG, VERSTR, VERDATE);
                printf(USAGE);
                exit(0);
                break;
        }
    }

    /* Load the preferences file to get additional configurations */
    loadConfig();
}

/*========================================================================
 * Name:   initConfig
 * Prototype:  void initConfig( void )
 *
 * Description:
 * Initialize run time configuration.  These are the default
 * settings.
 *========================================================================*/
void
initConfig( void )
{
    memset(&cliOptions, 0, sizeof(CLI_T));

    /* Enable the server */
    cliOptions.flags |= CLI_SERVER;

    /* Set the preferences file name */
    cliOptions.flags    |= CLI_FILE;
    cliOptions.filename  = F_CFG;
}


/*========================================================================
 * Name:   validateConfig
 * Prototype:  void validateConfig( void )
 *
 * Description:
 * Validate configuration file used as input
 *========================================================================*/
void
validateConfig( void )
{
}

/*========================================================================
 * Name:   isCLIFlagSet
 * Prototype:  void isCLIFlagSet( int )
 *
 * Description:
 * Checks to see if an option is set in cliOptions.flags using a thread lock.
 * 
 * Returns;
 * 0 if requested flag is not set.
 * 1 if requested flag is set.
 *========================================================================*/
int
isCLIFlagSet( int bits )
{
    int status = 0;

    if ( cliOptions.flags & bits )
        status = 1;

    return status;
}

/*========================================================================
 * Name:   setCLIFlag
 * Prototype:  void setCLIFlag( int )
 *
 * Description:
 * Set options is in cliOptions.flags using a thread lock.
 *========================================================================*/
void
setCLIFlag( int bits )
{
    pthread_mutex_lock( &cliOptionsMutex );
    cliOptions.flags |= bits;
    pthread_mutex_unlock( &cliOptionsMutex );
}

/*========================================================================
 * Name:   unsetCLIFlag
 * Prototype:  void unsetCLIFlag( int )
 *
 * Description:
 * Unset options is in cliOptions.flags using a thread lock.
 *========================================================================*/
void
unsetCLIFlag( int bits )
{
    pthread_mutex_lock( &cliOptionsMutex );
    cliOptions.flags &= ~bits;
    pthread_mutex_unlock( &cliOptionsMutex );
}

/*========================================================================
 * Name:   isPriv
 * Prototype:  int isPriv( char * )
 *
 * Description:
 * Checks to see if the specified appname is a privileged app.
 *
 * Returns
 * 0 if the appname is not privileged.
 * 1 if the appname is privileged.
 *========================================================================*/
int
isPriv( char *appname )
{
    PRIV_T *ptr = privHead;
    if ( appname == NULL )
        return 0;

    while (ptr != NULL)
    {
        if ( strcmp(ptr->appname, appname) == 0 )
            return 1;
        ptr = ptr->next;
    }
    return 0;
}

