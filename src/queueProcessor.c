/*******************************************************************************
 * PiBox application manager
 *
 * queueProcessor.c:  Process inbound messages.
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define QUEUEPROCESSOR_C

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <pthread.h>
#include <semaphore.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <ifaddrs.h>
#include <string.h>
#include <errno.h>
#include <uuid/uuid.h>
#include <sys/types.h>
#include <pwd.h>

#include "appmgr.h"

static int queueIsRunning = 0;
static pthread_mutex_t queueProcessorMutex = PTHREAD_MUTEX_INITIALIZER;
static pthread_t queueProcessorThread;

static char *currentPath = NULL;
static char *newPath     = NULL;
static pid_t currentPID  = -1;
static pid_t newPID      = -1;
static pid_t launcherPID = -1;

/*
 ******************************************************************************
 ******************************************************************************
 *
 * Message Type Handlers
 *
 ******************************************************************************
 ******************************************************************************
 */

/*========================================================================
 * Name:   getPath
 * Prototype:  char *getPath( PIBOX_MSG_T * )
 *
 * Description:
 * Pull the path from the payload and return it.
 *========================================================================*/
static char *
getPath( PIBOX_MSG_T *msg )
{
    char *payload;
    char *parsed;
    char *path;

    payload = strdup( msg->payload );
    piboxLogger(LOG_INFO, "getPath: payload = %s\n", payload);
    parsed = strtok( payload, " " );
    if ( parsed == NULL )
        return NULL;
    path = strdup( parsed );
    free(payload);
    return path;
}

/*========================================================================
 * Name:   signalLauncher
 * Prototype:  void signalLauncher( void )
 *
 * Description:
 * Notify the launcher that an app stopped.  This is a hack.  We really
 * should be sending a real message.
 *========================================================================*/
void
signalLauncher( void )
{
    /* The launcher will catch SIGUSR1 */
    if ( !isCLIFlagSet( CLI_TEST) && (launcherPID > 2) )
        kill(launcherPID, SIGUSR1);
}

/*========================================================================
 * Name:   stopLauncher
 * Prototype:  void stopLauncher( void )
 *
 * Description:
 * Try to stop the Launcher application, if any.
 *========================================================================*/
void
stopLauncher( void )
{
    struct stat stat_buf;
    char procbuf[256];
    int status;
    pid_t pid;

    piboxLogger(LOG_INFO, "Trying to stop launcher process.\n");
    if ( launcherPID != -1 )
    {
        piboxLogger(LOG_INFO, "Sending SIGTERM to %d\n", launcherPID);
        kill(launcherPID, SIGTERM);
        usleep(500000);
        pid = waitpid( pid, &status, WNOHANG);
        if ( pid != launcherPID )
        {
            piboxLogger(LOG_INFO, "Sending SIGINT to %d\n", launcherPID);
            kill(launcherPID, SIGINT);
            usleep(500000);
            pid = waitpid( pid, &status, WNOHANG);
            if ( pid != launcherPID )
            {
                piboxLogger(LOG_INFO, "Sending SIGKILL to %d\n", launcherPID);
                kill(launcherPID, SIGKILL);
                usleep(500000);
                pid = waitpid( pid, &status, WNOHANG);
            }
        }

        sprintf(procbuf, "/proc/%d", launcherPID);
        if ( stat(procbuf, &stat_buf) == 0 )
            piboxLogger(LOG_ERROR, "Failed to reap child process.\n");
        piboxLogger(LOG_INFO, "Resetting launcherPID \n");
        launcherPID = -1;
    }
    else
        piboxLogger(LOG_INFO, "No launcher PID.\n");
}

/*========================================================================
 * Name:   clearCurrent
 * Prototype:  void clearCurrent( void )
 *
 * Description:
 * Clear the current path and pid.  This gets called from waiter.c:waiter()
 * when an app exits on its own (which is how the launcher works).
 *========================================================================*/
void
clearCurrent( void )
{
    piboxLogger(LOG_INFO, "Clearing current.\n");
    currentPID = -1;
    if ( currentPath != NULL )
        free(currentPath);
    currentPath = NULL;

    /* Tell the launcher the app stopped. */
    if ( !isCLIFlagSet( CLI_TEST) )
        signalLauncher();
}

/*========================================================================
 * Name:   stopCurrent
 * Prototype:  void stopCurrent( void )
 *
 * Description:
 * Try to stop the current application, if any.
 *========================================================================*/
void
stopCurrent( void )
{
    struct stat stat_buf;
    char procbuf[256];
    int status;
    pid_t pid;

    piboxLogger(LOG_INFO, "Trying to stop current process.\n");
    if ( currentPID != -1 )
    {
        piboxLogger(LOG_INFO, "Sending SIGTERM to %d\n", currentPID);
        kill(currentPID, SIGTERM);
        usleep(500000);
        pid = waitpid( pid, &status, WNOHANG);
        if ( pid != currentPID )
        {
            piboxLogger(LOG_INFO, "Sending SIGINT to %d\n", currentPID);
            kill(currentPID, SIGINT);
            usleep(500000);
            pid = waitpid( pid, &status, WNOHANG);
            if ( pid != currentPID )
            {
                piboxLogger(LOG_INFO, "Sending SIGKILL to %d\n", currentPID);
                kill(currentPID, SIGKILL);
                usleep(500000);
                pid = waitpid( pid, &status, WNOHANG);
            }
        }

        sprintf(procbuf, "/proc/%d", currentPID);
        if ( stat(procbuf, &stat_buf) == 0 )
            piboxLogger(LOG_ERROR, "Failed to reap child process.\n");
        piboxLogger(LOG_INFO, "Resetting currentPID \n");
        currentPID = -1;
    }
    else
        piboxLogger(LOG_INFO, "No current PID.\n");

    if ( currentPath != NULL )
    {
        piboxLogger(LOG_INFO, "Freeing currentPath \n");
        free(currentPath);
    }
    currentPath = NULL;
}

/*========================================================================
 * Name:   parse
 * Prototype:  void parse( char *, char **, int )
 *
 * Description:
 * Parse a character string into an array of character tokens.
 *
 * Input Arguments:
 * char *line       The string to parse
 * char **argv      The parsed tokens
 *
 * Note:
 * Borrowed from http://www.csl.mtu.edu/cs4411.ck/www/NOTES/process/fork/shell.c
 *========================================================================*/
static void
parse(char *line, char **argv, int max)
{
    int idx = 0;

    /* if not the end of line ....... */ 
    while (*line != '\0') 
    {       
        /* replace white spaces with 0 */
        while (*line == ' ' || *line == '\t' || *line == '\n')
            *line++ = '\0';     

        /* save the argument position */
        if ( idx < max )
            *argv++ = line;

        /* skip the argument until ... */
        while (*line != '\0' && *line != ' ' && *line != '\t' && *line != '\n') 
            line++;

        if ( ++idx == max )
            break;
    }

    /* mark the end of argument list  */
    *argv = '\0';
}

/*========================================================================
 * Name:   startNew
 * Prototype:  int startNew( char * )
 *
 * Description:
 * Start a new application process.
 *========================================================================*/
static int
startNew( PIBOX_MSG_T *msg )
{
    pid_t pid;
    char *argv[64];
    char *str;
    struct stat stat_buf;
    struct passwd *pw;

    str = g_strdup(msg->payload);
    parse(str, argv, 64);

    // Verify the path exists 
    if ( stat(argv[0], &stat_buf) != 0 )
    {
        piboxLogger(LOG_ERROR, "No such file: %s\n", argv[0]);
        return 0;
    }

    pid = fork();
    if ( pid == 0 )
    {   
        // Change to user "nobody"
        if ( !isPriv(argv[0]) )
        {
            errno=0;
            pw = getpwnam("nobody");
            if ( pw != NULL )
            {
                setgid(pw->pw_gid);
                setuid(pw->pw_uid);
            }
            else
            {
                piboxLogger(LOG_ERROR, "Failed to change to user nobody: %s\n", strerror(errno));
                abort();
            }
        }

        // Exec the app.
        execvp(*argv, argv);
        piboxLogger(LOG_ERROR, "Failed to launch path: %s: reason = %s\n", argv[0], strerror(errno));
        abort();
    }

    // Save the PID and path
    if ( strstr(argv[0], "launcher") != NULL )
    {   
        launcherPID = pid;
        piboxLogger(LOG_INFO, "%s: Started launcher: pid/path = %d / %s\n", 
            __FUNCTION__, pid, msg->payload);
        g_free(str);
        return 1;
    }

    newPID = pid;
    setPID(newPID);
    newPath = strdup(argv[0]);
    piboxLogger(LOG_INFO, "%s: Started app: pid/path = %d / %s\n", __FUNCTION__, newPID, newPath);
    g_free(str);

    // Tell caller all is well.
    return 1;
}

#if 0
/*
 *========================================================================
 * Name:   notify
 * Prototype:  void notify( PIBOX_MSG_T )
 *
 * Description:
 * Sends a packet to the currently running app.
 *========================================================================
 */
void 
notify(PIBOX_MSG_T *msg, int hdr)
{      
    int                 sockfd = 0;
    int                 size;
    int                 n;
    struct sockaddr_in  serv_addr; 

    // Initialization of buffers
    memset(&serv_addr, '0', sizeof(serv_addr)); 

    // Create a socket for the message
    if((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        piboxLogger(LOG_ERROR, "Could not create socket to running app: %s\n", strerror(errno));
        return;
    } 

    // Setup TCP connection to app port (specified by app library)
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(APP_PORT); 

    // app must be running on localhost
    if(inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr)<=0)
    {
        piboxLogger(LOG_ERROR, "inet_pton error: %s\n", strerror(errno));
        return;
    } 

    // Connect to app
    if( connect(sockfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
    {
       piboxLogger(LOG_ERROR, "Connect Failed %s \n", strerror(errno));
       return;
    } 

    // 8bit size = length of command
    size = strlen(msg->payload);

    // Send it
    n = write(sockfd,&hdr,4);
    if (n < 0) 
    {
         piboxLogger(LOG_ERROR, "Failed writing hdr: %s", strerror(errno));
         return;
    }
    n = write(sockfd,&size,4);
    if (n < 0) 
    {
         piboxLogger(LOG_ERROR, "Failed writing size: %s", strerror(errno));
         return;
    }
    n = write(sockfd,msg->payload,size);
    if (n < 0) 
    {
         piboxLogger(LOG_ERROR, "Failed writing payload: %s", strerror(errno));
         return;
    }

    // Close the socket
    close(sockfd);
}
#endif

/*========================================================================
 * Name:   handler
 * Prototype:  int handler( PIBOX_MSG_T * )
 *
 * Description:
 * Process a get or set request related to network configuration.
 *========================================================================*/
static void
handler( PIBOX_MSG_T *msg )
{
    int msgType = -1;
    int doStart = 0;
    char *path  = NULL;

    // Safety check.
    if ( msg == NULL )
    {
        piboxLogger(LOG_TRACE3, "Message is NULL. \n");
        return;
    }

    // What type of message?
    msgType = msg->header & 0x000000ff;
    switch(msgType)
    {
        case MT_START:
            piboxLogger(LOG_TRACE3, "Received MT_START.\n");
            // If no app started, go ahead and try to start it.
            if ( currentPath == NULL )
            {
                piboxLogger(LOG_TRACE3, "Current path = NULL\n");
                startNew(msg);
                piboxLogger(LOG_INFO, "%s: Started new app: pid = %d\n", __FUNCTION__, newPID);
                currentPID = newPID;
                currentPath = newPath;
            }
            else
            {
                piboxLogger(LOG_TRACE3, "Current path != NULL\n");
                // Is the requested path different from current path.
                path = getPath(msg);
                if ( path!=NULL )
                {
                    piboxLogger(LOG_TRACE3, "getPath != NULL\n");
                    if ( strcmp(currentPath, path) != 0 )
                    {
                        // try and start it, then stop the current.
                        if ( startNew(msg) )
                            stopCurrent();
                        currentPID = newPID;
                        currentPath = newPath;
                    }
                    free(path);
                }
                else
                    piboxLogger(LOG_INFO, "%s(%d): No path for msg.\n", __FUNCTION__, __LINE__);
            }
            break;

        case MT_STOP:
            // Is the requested path running?  If so, stop it.
            piboxLogger(LOG_TRACE3, "Received MT_STOP.\n");
            if ( currentPath != NULL )
            {
                piboxLogger(LOG_INFO, "%s: trying to stop current app.\n", __FUNCTION__);
                path = getPath(msg);
                if ( path!=NULL )
                {
                    if ( strcmp(currentPath, path) == 0 )
                        stopCurrent();
                    else
                        piboxLogger(LOG_INFO, "%s: no match: current vs new: %s / %s\n", 
                            __FUNCTION__, currentPath, path);
                    free(path);
                }
                else
                    piboxLogger(LOG_INFO, "%s: path in message is null.\n", __FUNCTION__);
            }
            break;

#if 0
        case MT_KEY:
            // A remote control is sending us a keycode to pass to the currently running app.
            piboxLogger(LOG_TRACE3, "Received MT_KEY.\n");
            if ( currentPath != NULL )
            {
                if ( msg->payload != NULL )
                {
                    piboxLogger(LOG_INFO, "%s: forwarding keycode to app: %s\n", __FUNCTION__, msg->payload);
                    notify(msg, APP_MT_KEY);
                }
                else
                    piboxLogger(LOG_INFO, "%s: keycode in message is null.\n", __FUNCTION__);
            }
            break;
#endif

        default:
            piboxLogger(LOG_ERROR, "Unknown type: %d - ignoring message.\n", msgType);
            break;
    }

    // Cleanup 
    freeMsgNode( msg );
}

/*
 ******************************************************************************
 ******************************************************************************
 *
 * Thread functions
 *
 ******************************************************************************
 ******************************************************************************
 */

/*========================================================================
 * Name:   isQueueProcessorRunning
 * Prototype:  int isQueueProcessorRunning( void )
 *
 * Description:
 * Thread-safe read of queueIsRunning variable.
 *========================================================================*/
static int
isQueueProcessorRunning( void )
{
    int status;
    pthread_mutex_lock( &queueProcessorMutex );
    status = queueIsRunning;
    pthread_mutex_unlock( &queueProcessorMutex );
    return status;
}

/*========================================================================
 * Name:   setQueueProcessorRunning
 * Prototype:  int setQueueProcessorRunning( void )
 *
 * Description:
 * Thread-safe set of queueIsRunning variable.
 *========================================================================*/
static void
setQueueProcessorRunning( int val )
{
    pthread_mutex_lock( &queueProcessorMutex );
    queueIsRunning = val;
    pthread_mutex_unlock( &queueProcessorMutex );
}

/*========================================================================
 * Name:   queueProcessor
 * Prototype:  void queueProcessor( CLI_T * )
 *
 * Description:
 * Grab entries from queue and process them.
 *
 * Input Arguments:
 * void *arg    Cast to CLI_T to get run time configuration data.
 *
 * Notes:
 * Sleep between empty reads defaults to 100 milliseconds.
 *========================================================================*/
static void *
queueProcessor( void *arg )
{
    PIBOX_MSG_T *msg = NULL;
    int msgType = -1;

    /* Spin, looking for messages to process. */
    queueIsRunning = 1;
    while( isCLIFlagSet(CLI_SERVER) )
    {
        /* Block waiting for at least one message to be added to the queue. */
        sem_wait(&queueSem);

        /* Exhaust the queue. */
        if ( (msg = popMsgQueue()) != NULL ) {
            handler(msg);
        }
    }

    piboxLogger(LOG_INFO, "%s: queueProcessor thread is exiting.\n", PROG);
    queueIsRunning = 0;

    // Call return() instead of pthread_exit so valgrid doesn't report dlopen problems.
    // pthread_exit(NULL);
    return(0);
}

/*========================================================================
 * Name:   startQueueProcessor
 * Prototype:  void startQueueProcessor( void )
 *
 * Description:
 * Setup thread to handle inbound messages.
 *
 * Notes:
 * Threads run until configs->serverEnabled = 0.  See shutdownQueueProcessor().
 *========================================================================*/
void
startQueueProcessor( void )
{
    int rc;

    /* Set up the semaphore used to process inbound messages. */
    if (sem_init(&queueSem, 0, 0) == -1)
    {
        piboxLogger(LOG_ERROR, "Failed to get queue processor semaphore: %s\n", strerror(errno));
        return;
    }

    /* Create a thread to handle inbound messages. */
    rc = pthread_create(&queueProcessorThread, NULL, &queueProcessor, (void *)NULL);
    if (rc)
    {
        piboxLogger(LOG_ERROR, "%s: Failed to create processor thread: %s\n", PROG, strerror(rc));
        exit(-1);
    }
    piboxLogger(LOG_INFO, "%s: Started process thread.\n", PROG);
    return;
}

/*========================================================================
 * Name:   shutdownQueueProcessor
 * Prototype:  void shutdownQueueProcessor( void )
 *
 * Description:
 * Shut down message processing thread.
 *========================================================================*/
void
shutdownQueueProcessor( void )
{
    int timeOut = 1;

    stopCurrent();

    /* Wake sleeping processor, if necessary. */
    if ( sem_post(&queueSem) != 0 )
        piboxLogger(LOG_ERROR, "Failed to post queueSem semaphore.\n");

    while ( isQueueProcessorRunning() )
    {
        sleep(1);
        timeOut++;
        if (timeOut == 60)
        {
            piboxLogger(LOG_ERROR, "%s: Timed out waiting on processor thread to shut down.\n", PROG);
            return;
        }
    }
    pthread_detach(queueProcessorThread);

    /* Clean up the semaphore used for this loop. */
    sem_destroy(&queueSem);

    piboxLogger(LOG_INFO, "%s: queueProcessor has shut down.\n", PROG);
}
