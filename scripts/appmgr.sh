#!/bin/sh
# Interface to appmgr.
# Caller provides a path to an app and this script sends an appropriately
# formatted message to the application manager, appmgr.
#-------------------------------------------------------------------------
TESTDATA=/var/run/appmgr.data.$$
PORT=13912

# Start command
HDR=00000001

# Get path from caller.
path="$*"
if [ "$path" = "" ]
then
    exit 0
fi

size=`echo $path | wc -c`
size=`expr $size - 1`
printf "0: %s" $HDR | sed -E 's/0: (..)(..)(..)(..)/0: \4\3\2\1/' | xxd -r > $TESTDATA
printf "8: %.8x" $size | sed -E 's/8: (..)(..)(..)(..)/8: \4\3\2\1/' | xxd -r - - >> $TESTDATA
echo -ne "$path" >> $TESTDATA
nc localhost $PORT < $TESTDATA >/dev/null 2>&1
rm $TESTDATA
