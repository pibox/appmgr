#!/bin/bash 
# Test the server with good and bad data.
# ------------------------------------------------------

#--------------------------------------------------------------
# Initialization
#--------------------------------------------------------------
PORT=13912
TESTDATA=appmgr.data
LOGFILE=appmgr.log

# Test names
FORMAT=
TYPE=
APP=

# Test config variables
DIE=0
VERBOSE=0
KEEP=0
NCOUT=

#--------------------------------------------------------------
# Functions
#--------------------------------------------------------------
# Provide command line usage assistance
function doHelp
{
    echo ""
    echo "$0 [-akv | -i ids | -t name | -t ... ]"
    echo "where"
    echo "-a          Run all tests in the order listed below"
    echo "-v          Enable verbose output"
    echo "-k          Keep logfile"
    echo "-t name     name of test to enable, can be specified multiple times"
    echo "            Available tests:"
    echo "            Name        Description"
    echo "            format      Test invalid message formats"
    echo "            type        Test invalid requests"
    echo "            app         Test start/stop requests"
    echo "-i ids      Space separated list of subtests, enclosed in double quotes"
    echo "            Available subtests:"
    echo "            ID Test     Subtest"
    echo "            1  format   blank message"
    echo "            2  format   Missing header"
    echo "            3  format   Missing size"
    echo "            4  format   Missing path"
    echo "            5  app      Start and stop an xterm"
    echo "            6  app      Start an unknown application"
    echo "            7  app      Stop an application that isn't running"
    echo "            8  app      Start a second app"
    echo ""
}

# Generate file that contains a message to daemon.
function genMsg
{
    # Value should be a hex string
    header=$1

    # Value should be character string
    path=$2
    
    # Header
    if [ "$NOHDR" == "" ]
    then
        if [ "$BADHDR" != "" ]
        then
            echo "blah" > $TESTDATA
        else
            printf "0: %s" $header | sed -E 's/0: (..)(..)(..)(..)/0: \4\3\2\1/' | xxd -r > $TESTDATA
        fi
    fi

    # Size
    if [ "$NOSIZE" = "" ]
    then
        size=`echo $path | wc -c`
        size=`expr $size - 1`
        if [ "$BADSIZE" != "" ]
        then
            case $BADSIZE in
            1) size=255;;
            2) size=`expr $size - 1`;;
            3) size=0;;
            esac
        fi
        printf "8: %.8x" $size | sed -E 's/8: (..)(..)(..)(..)/8: \4\3\2\1/' | xxd -r - - >> $TESTDATA
    fi

    # Path
    if [ "$NOPATH" = "" ]
    then
        echo -ne $path >> $TESTDATA
    fi

    [ $VERBOSE -eq 1 ] && hexdump -C < $TESTDATA
}

# Send the message to the daemon
function sendIt
{
    if [ "$NCOUT" != "" ]
    then
        nc localhost $PORT < $TESTDATA 2>&1 | tee $DATAFILE
    else
        # nc localhost $PORT < $TESTDATA >/dev/null 2>&1
        nc localhost $PORT < $TESTDATA 
    fi
    rc=$?
    if [ "$RCBAD" == "" ]
    then
        case "$rc" in
        "0") ;;
        *)  echo "Test failed: rc = $rc"
            if [ $DIE -eq 1 ]
            then
                die $rc
            fi
            ;;
        esac
    else
        case "$rc" in
        "0") echo "No error but there should be!";;
        *)   ;;
        esac
    fi
    rm $TESTDATA
}

# Kill the server
function die
{
    rc=$1
    if [ "$SERVER_PID" != "" ]
    then
        echo "Stopping server pid $SERVER_PID"
        kill -s SIGINT $SERVER_PID
    fi
    cnt=0
    found=0
    while [ "$cnt" != "10" ]
    do
        grep "b'bye" $LOGFILE 2>&1 >/dev/null
        if [ $? -eq 0 ]
        then
            found=1
            break
        fi
        sleep 1
        cnt=`expr $cnt + 1`
    done
    if [ $found -eq 0 ]
    then
        echo "$0 failed to shutdown appmgr."
    fi

    # Clean up log file, unless requested not to.
    if [ $KEEP -eq 0 ]
    then
        rm -f $LOGFILE
    fi

    echo "$0 b'bye!"
    exit $rc
}

# Reset the server and logs 
function reset
{
    > $LOGFILE
}

# Test the log file for the specified string
# arguments
# 1:  doDie (if set, die on failures)
# 2:  expected (if set, string should be present)
# 3:  msg to test for
function logTest
{
    doDie=$1
    expected=$2
    msg=$3

    if [ $expected -eq 1 ]
    then
        rc=0
        badState="Missing"
    else
        rc=1
        badState="Found"
    fi

    grep "$msg" $LOGFILE 2>&1 >/dev/null
    if [ $? != $rc ]
    then
        echo "FAIL: $badState message: $msg"
        if [ $doDie -eq 1 ]
        then
            die 1
        fi
    fi
}

#--------------------------------------------------------------
# Read command line arguments
#--------------------------------------------------------------
while getopts ":akvi:t:" Option
do
    case $Option in
    t)  testname=`echo $OPTARG | tr a-z A-Z`
        eval $testname=1
        ;;
    i)  IDS="$OPTARG";;
    v)  VERBOSE=1;;
    k)  KEEP=1;;
    a)  FORMAT=1
        TYPE=1
        APP=1
        ;;
    *)  doHelp; exit 0;;
    esac
done

#--------------------------------------------------------------
# Start the server with logging to a file
#--------------------------------------------------------------

# Test for any running tests.
ps xa | grep appmgr | grep -v grep

rm -f $LOGFILE
SERVER=src/appmgr
if [ -f server.sh ]
then
    SERVER="../"$SERVER
fi
$SERVER -T -v3 -l$LOGFILE &
SERVER_PID=$!
echo "Server PID = $SERVER_PID"

echo "Waiting for server startup to complete..."
cnt=0
found=0
while [ "$cnt" != "3" ]
do
    grep "Spinning..." $LOGFILE 2>&1 >/dev/null
    if [ $? -eq 0 ]
    then
        found=1
        break
    fi
    sleep 1
    cnt=`expr $cnt + 1`
done
if [ $found -eq 0 ]
then
    echo "appmgr did not start."
    reset
    exit 1
fi

#--------------------------------------------------------------
# Tests
#--------------------------------------------------------------

echo "--------------------------------------------------------------"
echo "Starting Tests"
echo "--------------------------------------------------------------"

#--------------------------------------------------------------
# Message format tests
#--------------------------------------------------------------
if [ "$FORMAT" != "" ]
then
    [ $VERBOSE -eq 1 ] && echo "TYPE: invalid message types"

    if [ "$IDS" = "" ]
    then
        IDS="4 5 6 7"
    fi

    DIE=1

    # blank message
    subtest=`echo $IDS | grep 1`
    if [ "$subtest" != "" ]
    then
        [ $VERBOSE -eq 1 ] && echo "TYPE: blank message"
        reset
        echo "" > $TESTDATA
        sendIt
        sleep 1
        logTest 1 1 "Failed to get header"
    fi

    # Missing header
    subtest=`echo $IDS | grep 2`
    if [ "$subtest" != "" ]
    then
        [ $VERBOSE -eq 1 ] && echo "TYPE: missing header"
        reset
        NOHDR=1
        genMsg 00000004 /usr/bin/xterm
        unset NOHDR
        sendIt
        sleep 1
        logTest 1 1 "Failed to read payload"
    fi

    # Missing size
    subtest=`echo $IDS | grep 3`
    if [ "$subtest" != "" ]
    then
        [ $VERBOSE -eq 1 ] && echo "TYPE: missing size"
        reset
        NOSIZE=1
        genMsg 00000004 /usr/bin/xterm
        unset NOSIZE
        sendIt
        sleep 1
        logTest 1 1 "Failed to read payload"
    fi

    # Missing path
    subtest=`echo $IDS | grep 4`
    if [ "$subtest" != "" ]
    then
        [ $VERBOSE -eq 1 ] && echo "TYPE: missing path"
        reset
        NOPATH=1
        genMsg 00000004 /usr/bin/xterm
        unset NOPATH
        sendIt
        sleep 1
        logTest 1 1 "Failed to read payload"
    fi

    unset DIE
fi

#--------------------------------------------------------------
# Test type processing
#--------------------------------------------------------------
if [ "$TYPE" != "" ]
then
    [ $VERBOSE -eq 1 ] && echo "TYPE: invalid message types"

    # Send the following message to the server
    # This is valid except for the type field.
    # So it passes the message processor but the 
    # queue processor won't recognize it.
    reset
    genMsg 00000011 /usr/bin/xterm
    sendIt
    sleep 1
    logTest 1 1 "Unknown type"
fi

#--------------------------------------------------------------
# Start/stop app tests
#--------------------------------------------------------------
if [ "$APP" != "" ]
then
    if [ "$IDS" = "" ]
    then
        IDS="1 2 3"
    fi

    # Start and stop an xterm
    subtest=`echo $IDS | grep 5`
    if [ "$subtest" != "" ]
    then
        [ $VERBOSE -eq 1 ] && echo "APP: Start and stop an xterm"
        reset
        genMsg 00000001 /usr/bin/xterm
        sendIt
        sleep 1
        logTest 1 0 "No such file"
        logTest 1 0 "Failed to launch"
        logTest 1 1 "Started app"

        reset
        genMsg 00000002 /usr/bin/xterm
        sendIt
        sleep 1
        logTest 1 0 "Failed to reap"
        logTest 1 0 "No current PID"
        logTest 1 1 "Freeing currentPath"
    fi

    # Start an unknown application
    subtest=`echo $IDS | grep 6`
    if [ "$subtest" != "" ]
    then
        [ $VERBOSE -eq 1 ] && echo "APP: Start an unknown application"
        reset
        genMsg 00000001 /usr/bin/blah
        sendIt
        sleep 1
        logTest 1 1 "No such file"
    fi

    subtest=`echo $IDS | grep 7`
    if [ "$subtest" != "" ]
    then
        [ $VERBOSE -eq 1 ] && echo "APP: Stop an application that isn't running"
        reset
        genMsg 00000001 /usr/bin/xterm
        sendIt
        sleep 1
        logTest 1 1 "Started app"

        reset
        genMsg 00000002 /usr/bin/blah
        sendIt
        sleep 1
        logTest 0 1 "no match: current vs new"

        reset
        genMsg 00000002 /usr/bin/xterm
        sendIt
        sleep 1
        logTest 1 1 "Freeing currentPath"
    fi

    subtest=`echo $IDS | grep 8`
    if [ "$subtest" != "" ]
    then
        [ $VERBOSE -eq 1 ] && echo "APP: Start a second app"
        genMsg 00000001 /usr/bin/xterm
        sendIt
        sleep 1
        logTest 1 1 "Started app"

        reset
        genMsg 00000001 /usr/bin/xeyes
        sendIt
        sleep 1
        logTest 0 1 " Resetting currentPID"

        reset
        genMsg 00000002 /usr/bin/xterm
        sendIt
        sleep 1
        logTest 0 1 "no match: current vs new"

        reset
        genMsg 00000002 /usr/bin/xeyes
        sendIt
        sleep 1
        logTest 1 1 "Freeing currentPath"
    fi
fi

#--------------------------------------------------------------
# Reset for another round of tests.
#--------------------------------------------------------------
die 0

echo "Done."
