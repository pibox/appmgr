#!/bin/bash -p
# ------------------------------------------------------

#--------------------------------------------------------------
# Functions
#--------------------------------------------------------------
# Provide command line usage assistance
function doHelp
{
    echo ""
    echo "$0 [-r] "
    echo "where"
    echo "-r    Enable show-reachable"
    echo ""
    echo "Uses Valgrind to run the piboxd binary in test mode and check for memory leaks."
    echo "Should be run first, followed by unit tests."
    echo "Run this script from the top of the repository."
    echo "Use Ctrl-C to stop the test and get leak reports from Valgrind."
    echo ""
}

#--------------------------------------------------------------
# Read command line arguments
#--------------------------------------------------------------
REACHABLE=
while getopts ":r" Option
do
    case $Option in
    r)  REACHABLE="--show-reachable=yes";;
    *)  doHelp; exit 0;;
    esac
done

valgrind -v --tool=memcheck --leak-check=full $REACHABLE --leak-resolution=high \
	--num-callers=20 --suppressions=tests/gtk.suppression \
	src/appmgr -T -v3
